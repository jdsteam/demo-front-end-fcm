# Demo front end get push notification via FCM
The file is already configured with FCM key.


# Run the demo
Install NodeJS & NPM first.
```shell
npx http-server
```
then open [http://localhost:8080/](http://localhost:8080/)
Allow the notification.
Refresh the page.

to send sample notification, run:
```shell
./curl-test.sh
```

### Author

Ganjar Setia M.
